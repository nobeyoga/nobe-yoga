Nobe Yoga Miami Beach is a full service yoga studio servicing Miami Beach. We specialize in basic, kundalini, prenatal, hot yoga classes and many more. Nobe Yoga offers thai massage, acupuncture workshops and yoga training certifications. For yoga Miami Beach and North Miami Beach call 305-720-2077.

Address: 214 67th st, Miami Beach, FL 33141

Phone: 305-720-2077
